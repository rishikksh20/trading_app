import pandas as pd
import time
import matplotlib.pyplot as plt
import datetime
import numpy as np
import gc
import hparams as hp

def get_market_data(market, tag=True):
  market_data = pd.read_html("https://coinmarketcap.com/currencies/" + market +
                             "/historical-data/?start=20130428&end="+time.strftime("%Y%m%d"), flavor='html5lib')[0]
  market_data = market_data.assign(Date=pd.to_datetime(market_data['Date']))
  market_data['Volume'] = (pd.to_numeric(market_data['Volume'], errors='coerce').fillna(0))
  if tag:
    market_data.columns = [market_data.columns[0]] + [tag + '_' + i for i in market_data.columns[1:]]
  return market_data


def merge_data(a, b, from_date=hp.merge_date):
  merged_data = pd.merge(a, b, on=['Date'])
  merged_data = merged_data[merged_data['Date'] >= from_date]
  return merged_data

def create_model_data(data):
  data = data[['Date']+[coin+metric for coin in ['BTC_', 'ETH_'] for metric in ['Close**','Volume']]]
  data = data.sort_values(by='Date')
  return data

def create_model_high_data(data):
  data = data[['Date']+[coin+metric for coin in ['BTC_', 'ETH_'] for metric in ['High','Volume']]]
  data = data.sort_values(by='Date')
  return data


def split_data(data, training_size=hp.training_size):
  return data[:int(training_size*len(data))], data[int(training_size*len(data)):]


def create_inputs(data, coins=['BTC', 'ETH'], window_len=hp.window_len):
  norm_cols = [coin + metric for coin in coins for metric in ['_Close**', '_Volume']]
  inputs = []
  for i in range(len(data) - window_len):
    temp_set = data[i:(i + window_len)].copy()
    inputs.append(temp_set)
    for col in norm_cols:
      inputs[i].loc[:, col] = inputs[i].loc[:, col] / inputs[i].loc[:, col].iloc[0] - 1
  return inputs

def create_inputs_high(data, coins=['BTC', 'ETH'], window_len=hp.window_len):
  norm_cols = [coin + metric for coin in coins for metric in ['_High', '_Volume']]
  inputs = []
  for i in range(len(data) - window_len):
    temp_set = data[i:(i + window_len)].copy()
    inputs.append(temp_set)
    for col in norm_cols:
      inputs[i].loc[:, col] = inputs[i].loc[:, col] / inputs[i].loc[:, col].iloc[0] - 1
  return inputs

def create_outputs(data, coin, window_len=hp.window_len):
  return (data[coin + '_Close**'][window_len:].values / data[coin + '_Close**'][:-window_len].values) - 1

def create_outputs_high(data, coin, window_len=hp.window_len):
  return (data[coin + '_High'][window_len:].values / data[coin + '_High'][:-window_len].values) - 1

def to_array(data):
  x = [np.array(data[i]) for i in range (len(data))]
  return np.array(x)
