import h5py
import pandas as pd
import time
import datetime
import numpy as np
import data_load as data
from keras.models import load_model

btc_model_high.load('./btc_model_high.h5')

eth_data = data.get_market_data("ethereum", tag='ETH')
btc_data = data.get_market_data("bitcoin", tag='BTC')
utils.show_plot(btc_data, tag='BTC')
utils.show_plot(eth_data, tag='ETH')
market_data = data.merge_data(btc_data, eth_data)
model_data_high = data.create_model_data(market_data)
X_test_latest = model_data_high[943:947]
X_test_latest = X_test_latest.drop('Date', 1)
X_test_latest = data.create_inputs_high(X_test_latest)
X_test_latest = data.to_array(X_test_latest)
# Returns a compiled model identical to the previous one
change_btc_high = btc_model_high.predict(X_test_latest)

previous_price = model_data_high[943:947]['BTC_High'].mean()
predict_price = previous_price + previous_price*change_btc_high
print("Prectited High Price for 5th Aug", predict_price)
print("Actual Price for 5th:\n")
print(model_data_high[948:949])
