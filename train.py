import h5py
import pandas as pd
import time
import matplotlib.pyplot as plt
import datetime
import numpy as np
import gc
import keras
from keras.models import Sequential
from keras.layers import Activation, Dense
from keras.layers import LSTM
from keras.layers import Dropout
import data_load as data
import model
import utils
import hparams as hp

'''
if ticker=='BTC':
    _data=data.get_market_data("bitcoin", tag='BTC')
    #btc_data = data.get_market_data("bitcoin", tag='BTC')
    utils.show_plot(btc_data, tag='BTC')
'''

eth_data = data.get_market_data("ethereum", tag='ETH')
btc_data = data.get_market_data("bitcoin", tag='BTC')
utils.show_plot(btc_data, tag='BTC')
utils.show_plot(eth_data, tag='ETH')
market_data = data.merge_data(btc_data, eth_data)


if hp.mode =='CLOSE':
    model_data = data.create_model_data(market_data)
    train_set, test_set = split_data(model_data)
    train_set = train_set.drop('Date', 1)
    test_set = test_set.drop('Date', 1)


    _train = data.create_inputs(train_set)
    _test = data.create_inputs(test_set)
    _train_btc = data.create_outputs(train_set, coin='BTC')
    _test_btc = data.create_outputs(test_set, coin='BTC')
    #_train_eth = data.create_outputs(train_set, coin='ETH')
    #_test_eth = data.create_outputs(test_set, coin='ETH')

    _train, _test = data.to_array(_train), data.to_array(_test)

    print("Shape of Arrays :\n")
    print(np.shape(_train), np.shape(_test))
    print (np.shape(_train_btc), np.shape(_test_btc))
    #print (np.shape(_train_eth), np.shape(_test_eth))

    gc.collect()
    np.random.seed(202)
    btc_model = model.build_model(_train, output_size=1, neurons=hp.neurons)
    btc_history = btc_model.fit(_train, _train_btc, epochs=hp.epochs, batch_size=hp.batch_size, verbose=1, validation_data=(_test, _test_btc), shuffle=False)

    btc_model.save('btc_model_close.h5')

if hp.mode == 'HIGH':

    model_data_high = data.create_model_high_data(market_data)
    train_set_high, test_set_high = data.split_data(model_data_high)
    train_set_high = data.train_set_high.drop('Date', 1)
    test_set_high = data.test_set_high.drop('Date', 1)

    _train_high = data.create_inputs_high(train_set_high)
    _train_btc_high = data.create_outputs_high(train_set_high, coin='BTC')
    _test_high = data.create_inputs_high(test_set_high)
    _test_btc_high = data.create_outputs_high(test_set_high, coin='BTC')

    _train_high, _test_high = data.to_array(_train_high), data.to_array(_test_high)

    print(np.shape(_train_high), np.shape(_test_high))
    print(np.shape(_train_btc_high), np.shape(_test_btc_high))

    gc.collect()
    np.random.seed(202)
    btc_model_high = build_model(_train_high, output_size=1, neurons=hp.neurons)
    btc_history = btc_model_high.fit(_train_high, _train_btc_high, epochs=hp.epochs, batch_size=hp.batch_size, verbose=1, validation_data=(_test_high, _test_btc_high), shuffle=False)
    btc_model_high.save('btc_model_high.h5')
